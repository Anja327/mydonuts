package com.example.myDonuts

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myDonuts.adapter.RvDonutsAdapter
import com.example.myDonuts.databinding.FragmentHomeBinding
import com.example.myDonuts.models.Donuts
import com.google.firebase.database.*

class HomeFragment : Fragment() {

    private var _binding : FragmentHomeBinding? = null
    private  val binding get() = _binding!!

    private lateinit var donutsList: ArrayList<Donuts>
    private lateinit var  firebaseRef : DatabaseReference
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater , container, false)

        binding.btnAdd.setOnClickListener{
            findNavController().navigate(R.id.action_homeFragment_to_addFragment)
        }

        firebaseRef = FirebaseDatabase.getInstance().getReference("donuts")
        donutsList = arrayListOf()

        fetchData()

        binding.rvDonuts.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this.context)
        }

        return binding.root
    }

    private fun fetchData() {
        firebaseRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                donutsList.clear()
                if (snapshot.exists()){
                    for (contactSnap in snapshot.children){
                        val donuts = contactSnap.getValue(Donuts::class.java)
                        donutsList.add(donuts!!)
                    }
                }
                val rvAdapter = RvDonutsAdapter(donutsList)
                binding.rvDonuts.adapter = rvAdapter
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context," error : $error",Toast.LENGTH_SHORT).show()
            }

        })
    }
}